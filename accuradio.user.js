// ==UserScript==
// @name         AccuRadio Save
// @version      0.1
// @description  Saves accuradio songs automatically
// @author       Miosame
// @match        https://www.accuradio.com/*
// @grant        GM_xmlhttpRequest
// ==/UserScript==

(function() {
    'use strict';
    let previous;

    setInterval(() => {
        let art_elem = document.querySelector("#albumArtImg");
        let artist_elem = document.querySelector("#songartist");
        let song_elem = document.querySelector("#songtitle");
        if(!art_elem || !artist_elem || !song_elem) return;

        let art = art_elem.src;
        let artist = artist_elem.innerText;
        let song = song_elem.innerText;
        if(!art || !artist || !song) return;
        if(previous === art+artist+song) return;

        GM_xmlhttpRequest({
            method: "POST",
            url: "http://localhost:33222/",
            data: JSON.stringify({art,artist,song}),
            onload: function(response) {
                previous = art+artist+song;
            }
        });
    },3000);
})();
