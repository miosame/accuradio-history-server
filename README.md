# accuradio-history-server

saves all songs with artist and album art you've listened to in your browser to a local html file

# requirements

- jq
- tampermonkey in firefox/chrome
- bash (plenty bashisms I'm sure)

# setup

1. install the tampermonkey script - accuradio.user.js
2. make the server executable `chmod +x accuradio-server.sh`
3. execute it via a startup script or manually at first to check it works, it should drop a `history.html` in the same folder as the server once it receives a request every 3 seconds from the tampermonkey script
