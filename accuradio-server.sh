#!/usr/bin/env bash

if ! [ -x "$(command -v jq)" ]; then
  echo 'Error: jq is not installed.' >&2
  exit 1
fi

# fuck absolute paths not resolving to actual file directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

while true; do 
    BODY=$(echo -e "HTTP/1.1 200 OK\n\n OK" | nc -l -p 33222 -q 1 | tail -n1)
    ARTIST=$(echo "${BODY}" | jq -r '.artist');
    SONG=$(echo "${BODY}" | jq -r '.song');
    ART=$(echo "${BODY}" | jq -r '.art');

    echo "
        <table style='border-collapse:separate; border-spacing: 50px;'>
            <tr style='text-align:center'>
                <td>album</td>
                <td>artist</td>
                <td>song</td>
            </tr>
            <tr>
                <td>
                    <img src='${ART}' width='100' />
                </td>
                <td>${ARTIST}</td>
                <td>${SONG}</td>
            </tr>
        </table>

        $(cat history.html)
    " > history.html
done
